<?php

class Sockets {
            
    public function __construct() {
        $this->createServer();
        $this->createUDPServer();

    }
    
    public function createServer() {
        if ($this->data->sockets->server = stream_socket_server("tcp://".$this->data->config->listenServer->bindAddress.":".$this->data->config->listenServer->bindPort)) {
            $this->debugger->log("INFO", "Listen server started on {$this->data->config->listenServer->bindAddress} using port {$this->data->config->listenServer->bindPort}.");
        } else {
            $this->debugger->log("ERROR", "Could not start listening server.");
        }
        
        if (stream_set_blocking($this->data->sockets->server, 0)) {
            $this->debugger->log("INFO", "Steam blocking disabled on listening server.");
        } else {
            $this->debugger->log("WARNING", "Cannot set stream blocking on server.");
        }
        $this->data->sockets->clients = array();
    }
    
    public function createUDPServer() {
        if ($this->data->sockets->udpServer = stream_socket_server("udp://".$this->data->config->listenServer->bindUDPAddress.":".$this->data->config->listenServer->bindUDPPort, $errno, $errstr, STREAM_SERVER_BIND)) {
            $this->debugger->log("INFO", "Listen server started on {$this->data->config->listenServer->bindUDPAddress} using port {$this->data->config->listenServer->bindUDPPort}.");
        } else {
            $this->debugger->log("ERROR", "Could not start listening server.");
        }
        
        if (stream_set_blocking($this->data->sockets->udpServer, 0)) {
            $this->debugger->log("INFO", "Steam blocking disabled on listening server.");
        } else {
            $this->debugger->log("WARNING", "Cannot set stream blocking on server.");
        }
        $this->data->sockets->clients = array();
    }
    
    public function connect() {
        $this->data->sockets->irc = stream_socket_client(($this->data->config->irc->ssl ? "ssl" : "tcp")."://".$this->data->config->irc->server.":".$this->data->config->irc->port, $errno, $errstr);
        
        if ($errstr) {
            $this->debugger->log("ERROR", "Could not connect to IRC.");
            return false;
        } else {
            $this->debugger->log("INFO", "Connected to {$this->data->config->irc->server} on port {$this->data->config->irc->port}.");
            return true;
        }
        
        if (stream_set_blocking($this->data->sockets->irc, 0)) {
            $this->debugger->log("INFO", "Steam blocking disabled on IRC client.");
        } else {
            $this->debugger->log("WARNING", "Cannot set stream blocking on IRC client.");
        }
        
    }
    
    public function disconnect() {
        
    }
    
    public function read(&$socket) {
        $buffer = fgets($socket);
        return $buffer;
    }
    
    public function udpRead(&$socket) {
        $buffer = stream_socket_recvfrom($socket, 2048);
        return $buffer;
    }
    
    public function write(&$socket, $message) {
        fwrite($socket, $message."\r\n");
    }
    
}

?>
