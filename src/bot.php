<?php

class Bot extends IRC {

    const BASEPATH = ".";
    public static $users;
    public static $cache;

    public function __construct() {

        // Global namespace.
        $this->data = "";

        // Configuration loading
        require_once(self::BASEPATH . "/inc/config.php");
        $this->data->config = $config;

        // Load the debugger
        $this->debugger = new Debugger($this->data->config->debug);

        // Load the module system
        Modules::initializeModules();

        parent::__construct();
        $this->listener();
    }

    public function listener() {
        while (1) {

            $sockets = array();
            $sockets[] = $this->data->sockets->irc;
            $sockets[] = $this->data->sockets->server;
            $sockets[] = $this->data->sockets->udpServer;
            foreach ($this->data->sockets->clients as $client) {
                $sockets[] = $client;
            }

            if (false === ($num_changed_streams = stream_select($sockets, $write = NULL, $except = NULL, 0))) {
                continue;
            } elseif ($num_changed_streams > 0) {
                foreach ($sockets as $socket) {
                    if ($socket == $this->data->sockets->server) {
                        // I don't like how this works, but, no fucks are given.
                        if ($client = @stream_socket_accept($socket, 200000)) {
                            $this->data->sockets->clients[] = $client;
                            $this->debugger->log("INFO", "Client {$client} connected.");
                        }
                    } elseif ($socket == $this->data->sockets->irc) {
                        $this->handleIRCBuffer();
                    } elseif ($socket == $this->data->sockets->udpServer) {
                        Games::handleGameBuffer();
                    } else {
                        $this->data->buffer->client = $this->read($client);
                        if (empty($this->data->buffer->client)) {
                            $client = array_search($socket, $this->data->sockets->clients);
                            $this->debugger->log("INFO", "Client {$client} disconnected.");
                            unset($this->data->sockets->clients[$client]);
                        } else {
                            $this->debugger->log("EVERYTHING", "CLIENT: {$this->data->buffer->client}");
                            if (preg_match("/TWEET--:(.*)/", $this->data->buffer->client, $matches)) {
                                IRC::MSG("#kaos", $matches[1]);
                            }
                            IRC::MSG("#kaosbot", "CLIENT: {$this->data->buffer->client}");
                        }
                    }
                }
            }
        }
    }

    public function sendToModules(&$modData) {
        Modules::handleEvent($modData);
    }

    public function cURL($url, $returnMode = "xml") {
        $curlOptions = array(
            CURLOPT_FORBID_REUSE => TRUE,
            CURLOPT_FRESH_CONNECT => TRUE,
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_DNS_CACHE_TIMEOUT => 2,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_RETURNTRANSFER => TRUE
        );

        $curl = curl_init($url);
        curl_setopt_array($curl, $curlOptions);
        $curlData = curl_exec($curl);
        $curlInfo = curl_getinfo($curl);
        curl_close($curl);

        print_r($curlInfo);
        
        if ($curlInfo["http_code"] != 503) {
            if ($curlData) {
                if ($returnMode == "xml") {
                    $returnData = new SimpleXmlElement($curlData, LIBXML_NOCDATA);
                } elseif ($returnMode == "json") {
                    $returnData = json_decode($curlData);
                } elseif ($returnMode == "string") {
                    $returnData = $curlData;
                }
            }
        } else {
            return false;
        }

        return $returnData;
    }

    public function kaosAPI($class, $function, $args) {
        $url = "{$this->data->config->api->url}/$class/$function";
        $dataObj = new stdClass();
        $dataObj->apiKey = $this->data->config->api->apiKey;
        
        foreach ($args as $key => $value) {
            $dataObj->$key = $value;
        }

        $jsonObj = json_encode($dataObj);
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Content-type: application/json'),
            CURLOPT_POSTFIELDS => $jsonObj,
            CURLOPT_FORBID_REUSE => TRUE,
            CURLOPT_FRESH_CONNECT => TRUE,
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_DNS_CACHE_TIMEOUT => 2,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_RETURNTRANSFER => TRUE
        );

        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        
        $result = json_decode($result);
        
        if ($result->status == 1) {
            return $result;
        } else {
            return $result;
        }
        
        
    }

}

?>
