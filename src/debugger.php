<?php

class Debugger {

    public function __construct($debugLevel) {
        $this->eventFile = "logs/events.txt";
        $this->debugLevel = $debugLevel;
    }

    public function log($type, $message) {

        if (($this->debugLevel > 0)) {
            
        }

        if (($this->debugLevel >= 1) && ($type == "ERROR")) {
            echo "[{$type}] $message\r\n";
        }

        if (($this->debugLevel >= 2) && ($type == "WARNING")) {
            echo "[{$type}] $message\r\n";
        }

        if (($this->debugLevel >= 3) && ($type == "INFO")) {
            echo "[{$type}] $message\r\n";
        }

        if (($this->debugLevel >= 4) && ($type == "EVERYTHING")) {
            echo "[{$type}] $message\r\n";
        }
    }
    
    public function logEvent($string) {
        $eventLog = file_get_contents($this->eventFile);
        file_put_contents($this->eventFile, $eventLog."\r\n\r\n\r\n".$string);
    }

}

?>
