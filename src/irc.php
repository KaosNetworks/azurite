<?php

class IRC extends Sockets {

    public function __construct() {
        global $bot;
        parent::__construct();
        $this->connectToIRC();
        $this->data->extraction->irc->config = $this->data->config;
    }

    public function connectToIRC() {
        if ($this->connect()) {
            echo $this->data->sockets->irc;
            $this->write($this->data->sockets->irc, "USER {$this->data->config->bot->ident} 0 * : {$this->data->config->bot->realName}");
            $this->write($this->data->sockets->irc, "NICK {$this->data->config->bot->nick}");
            if (!empty($this->data->config->irc->password)) {
                $this->write($this->data->sockets->irc, "PASS {$this->data->config->irc->password}");
            }
        }
    }

    public function handleIRCBuffer() {
        $this->data->buffer->irc = $this->read($this->data->sockets->irc);
        $this->debugger->log("EVERYTHING", "IRC: {$this->data->buffer->irc}");

        $bufferBits = explode(" ", $this->data->buffer->irc);

        // BEGIN IRC EVENT DETECTION AND PARSING
        if (preg_match("/:(.*)!(.*)@(.*)/", $bufferBits[0], $matches)) {
            $this->data->extraction->irc->person->nick = $matches[1];
            $this->data->extraction->irc->person->ident = $matches[2];
            $this->data->extraction->irc->person->host = $matches[3];

            if (is_object(Bot::$users[$matches[2] . "@" . $matches[3]])) {
                $this->data->extraction->irc->person->isLoggedIn = true;
                foreach (Bot::$users[$matches[2] . "@" . $matches[3]] as $key => $value) {
                    $this->data->extraction->irc->person->$key = $value;
                }
            } else {
                $this->data->extraction->irc->person->isLoggedIn = false;
            }
        }

        switch ($this->data->buffer->irc) {

            // PING
            case (preg_match("/PING (.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "IRC_PING";
                $this->write($this->data->sockets->irc, "PONG {$matches[1]}");
                $this->sendToModules($this->data->extraction->irc);
                break;

            // PRIVMSG
            case (preg_match("/:.* PRIVMSG (.*) :(.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "_PRIVMSG";
                $this->data->extraction->irc->channel = $matches[1];
                $this->data->extraction->irc->message = trim($matches[2]);
                $this->sendToModules($this->data->extraction->irc);
                break;

            // NOTICE
            case (preg_match("/:.* NOTICE (.*) :(.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "_NOTICE";
                $this->data->extraction->irc->channel = $matches[1];
                $this->data->extraction->irc->message = $matches[2];
                $this->sendToModules($this->data->extraction->irc);
                break;

            // JOIN
            case (preg_match("/:.* JOIN :(.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "_JOIN";
                $this->data->extraction->irc->channel = $matches[1];
                $this->sendToModules($this->data->extraction->irc);
                break;

            // PART
            case (preg_match("/:.* PART :(.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "_PART";
                $this->data->extraction->irc->channel = $matches[1];
                $this->sendToModules($this->data->extraction->irc);
                break;

            // KICK
            case (preg_match("/:.* KICK (\#.*) (.*) :(.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "_KICK";
                $this->data->extraction->irc->channel = $matches[1];
                $this->data->extraction->irc->kickee = $matches[2];
                $this->data->extraction->irc->reason = $matches[3];
                $this->sendToModules($this->data->extraction->irc);
                break;

            // QUIT
            case (preg_match("/:.* QUIT (.*)/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "_QUIT";
                $this->data->extraction->irc->message = $matches[1];
                $this->sendToModules($this->data->extraction->irc);
                break;

            // END OF MOTD
            case (preg_match("/:.* 422 .*/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "IRC_EVENT_422";
                $this->sendToModules($this->data->extraction->irc);
                break;

            // LIST ENTRIES
            case (preg_match("/:.* 322 {$this->data->config->bot->nick} (.*) ([0-9]+) :\[\+([a-zA-Z\s0-9]+)\] (.*).*/", $this->data->buffer->irc, $matches) ? true : false):
                $this->data->extraction->irc->function = "IRC_EVENT_322";
                $this->data->extraction->irc->channel = $matches[1];
                $this->data->extraction->irc->users = $matches[2];
                $this->data->extraction->irc->modes = $matches[3];
                $this->data->extraction->irc->topic = $matches[4];
                $this->sendToModules($this->data->extraction->irc);
                break;

            // WHO EENTRIES
            // :Aura.KaosNetworks.Com 352 PolyDebug #polyCraft PolyBot KaosIdentifier-a7l.vrc.g59c1b.IP Aura.KaosNetworks.Com PolyBot H :0 PolyBot
            case (preg_match("/:.* 352 .* (.*) (.*) (.*) .* (.*) (.*) :.*/", $this->data->buffer->irc, $matches) ? true : false):
                print_r($matches);
                if ($matches[1] != "") {
                    $this->data->extraction->irc->function = "IRC_EVENT_352";
                    $this->data->extraction->irc->nick = $matches[4];
                    $this->data->extraction->irc->channel = $matches[1];
                    $this->data->extraction->irc->ident = $matches[2];
                    $this->data->extraction->irc->host = $matches[3];
                    $this->data->extraction->irc->mode = $matches[5];
                    $this->sendToModules($this->data->extraction->irc);
                }
                break;

            // NAMES ENTRIES
            case (preg_match("/:.* 353 .* [=@] (.*) :(.*)/", $this->data->buffer->irc, $matches) ? true : false):
                if ($matches[1] != "") {
                    $this->data->extraction->irc->function = "IRC_EVENT_353";
                    $this->data->extraction->irc->channel = trim($matches[1]);
                    $users = array();
                    $users = explode(" ", trim($matches[2]));
                    $this->data->extraction->irc->users = $users;
                    $this->sendToModules($this->data->extraction->irc);
                }
                break;

            // QUIT
            case (preg_match("/ERROR :Closing link: \(.*\) \[(.*)\]/", $this->data->buffer->irc, $matches) ? true : false):
                die();
                break;

            default:
                $this->debugger->logEvent("{$this->data->buffer->irc}");
                $this->debugger->log("WARNING", "FAILED PARSE: {$this->data->buffer->irc}");
                break;
        }
    }

    /**
     * Sends a PRIVMSG to a channel/nick on IRC.
     * @param string $destination Channel/User to send PRIVMSG to. (Required)
     * @param string $message Message to send send. (Required)
     */
    public function MSG($destination, $message) {
        $message = str_split($message, 500);
        foreach ($message as $line) {
            Sockets::write($this->data->sockets->irc, "PRIVMSG $destination :$line");
        }
    }

    /**
     * Sends a NOTICE to a channel/nick on IRC.
     * @param string $destination Channel/User to send PRIVMSG to. (Required)
     * @param string $message Message to send send. (Required)
     */
    public function NOTICE($destination, $message) {
        $message = str_split($message, 500);
        foreach ($message as $line) {
            Sockets::write($this->data->sockets->irc, "NOTICE $destination :$line");
        }
    }

    /**
     * Joins a channel with optional password.
     * @param string $channel Channel to join. (Required)
     * @param string $password Password for +k channels. (Optional)
     */
    public function JOIN($channel, $password = null) {
        Sockets::write($this->data->sockets->irc, "JOIN $channel :$password");
    }

    /**
     * Sends an action to a user/channel. 
     * @param string $channel Channel/User to send action to. (Required)
     * @param string $message Action to perform. (Required)
     */
    public function ACTION($channel, $message) {
        Sockets::write($this->data->sockets->irc, "PRIVMSG $channel :ACTION $message");
    }

    /**
     * Parts a channel with option message.
     * @param string $channel Channel to part. (Required)
     * @param string $message Reason for parting. (Optional)
     */
    public function PART($channel, $message = "Parting...") {
        Sockets::write($this->data->sockets->irc, "PART $channel :$message");
    }

    /**
     * Invites a user into channel.
     * @param string $channel Channel to invite a user to. (Required)
     * @param string $nick User to invite. (Required)
     */
    public function INVITE($channel, $nick) {
        Sockets::write($this->data->sockets->irc, "INVITE $nick :$channel");
    }

    /**
     * Kicks specified user with option message.
     * @param string $channel channel to kick user from. (Required)
     * @param string $nick User to kick. (Required)
     * @param string $message Reason for kicking. (Optional)
     */
    public function KICK($channel, $nick, $message = "Kicked") {
        Sockets::write($this->data->sockets->irc, "KICK $channel $Who :$message");
    }

    /**
     * Sets mode on channel.
     * @param string $channel Channel to set mode(s) on. (Required)
     * @param string $mode Mode to set (Required)
     * @param string $params Optional params. (Optional)
     */
    public function MODE($channel, $mode, $params = "") {
        Sockets::write($this->data->sockets->irc, "MODE $channel $mode $params");
    }

    /**
     * Sets mode on channel. (SUPERADMIN)
     * @param string $channel Channel to set mode(s) on. (Required)
     * @param string $mode Mode to set (Required)
     * @param string $params Optional params. (Optional)
     */
    public function SAMODE($channel, $mode, $params = "") {
        Sockets::write($this->data->sockets->irc, "SAMODE $channel $mode $params");
    }

    /**
     * Changes the nick of the bot.
     * @param string $newNick New nickname. (Required)
     */
    public function NICK($newNick) {
        Sockets::write($this->data->sockets->irc, "NICK $newNick");
    }

    /**
     * Makes the bot quit.
     * @param string $message Reason. (Optional) 
     */
    public function QUIT($message = "Quiting...") {
        Sockets::write($this->data->sockets->irc, "QUIT $message");
    }

    /**
     * Sets the topic of channel.
     * @param string $channel Channel to set topic on. (Required)
     * @param string $topic Topic to set. (Required)
     */
    public function TOPIC($channel, $topic) {
        Sockets::write($this->data->sockets->irc, "TOPIC $channel :$topic");
    }

    /**
     * Sends a raw string to IRC.
     * @param string $string Raw string. (Required)
     */
    public function RAW($string) {
        Sockets::write($this->data->sockets->irc, "$string");
    }

    /**
     * Bans and kicks user from channel.
     * @param string $channel Channel to kick user from. (Required)
     * @param string $nick User to kick. (Required)
     * @param string $message Reason for kicking. (Optional)
     */
    public function BAN($channel, $nick, $message) {
        Sockets::write($this->data->sockets->irc, "MODE $channel +b :$nick");
        IRC::KICK($channel, $nick, $message);
    }

    /**
     * Sends a who information request.
     * @param string $target Channel name or user nickname to request data on. (Required)
     */
    public function WHO($target) {
        Sockets::write($this->data->sockets->irc, "WHO $target");
    }

}

?>
