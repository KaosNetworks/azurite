<?php

class Games extends Sockets {

    public function handleGameBuffer() {
        $this->data->buffer->udpServer = $this->udpRead($this->data->sockets->udpServer);
        $this->debugger->log("EVERYTHING", "GAME: {$this->data->buffer->udpServer}");

        $bufferBits = explode(" ", $this->data->buffer->udpServer);

        // BEGIN IRC EVENT DETECTION AND PARSING
        if (preg_match("/\"(.*)<(.*)><(.*)><(.*)>\"/", $this->data->buffer->udpServer, $matches)) {
            $this->data->extraction->game->person = array(
                "name" => $matches[1],
                "id" => $matches[2],
                "steamID" => $matches[3],
                "isTeamTalk" => ($matches[4] == "" ? 0 : 1)
            );
        }
        switch ($this->data->buffer->udpServer) {
            
            // SAY
            case (preg_match("/say \"(.*)\"/", $this->data->buffer->udpServer, $matches) ? true : false):
                $this->data->extraction->game->function = "_SAY";
                $this->data->extraction->game->message = trim($matches[1]);
                $this->sendToModules($this->data->extraction->game);
                break;
            
            case (preg_match("/entered the game/", $this->data->buffer->udpServer, $matches) ? true : false):
                $this->data->extraction->game->function = "_CONNECTED";
                $this->sendToModules($this->data->extraction->game);
                break;
            
            case (preg_match("/disconnected \(reason \"(.*)\"\)/", $this->data->buffer->udpServer, $matches) ? true : false):
                $this->data->extraction->game->function = "_DISCONNECTED";
                $this->data->extraction->game->message = trim($matches[1]);
                $this->sendToModules($this->data->extraction->game);
                break;

            
            default:
                $this->debugger->logEvent("{$this->data->buffer->udpServer}");
                $this->debugger->log("WARNING", "FAILED PARSE: {$this->data->buffer->udpServer}");
                break;
        }
    }

}

?>
