<?php

class Modules extends Bot {
          
    public function initializeModules(&$data) {
        $this->data->modules = new stdClass();
        $pathToModules = Bot::BASEPATH."/modules/";
        $scannedDir = array_diff(scandir($pathToModules), array('..', '.'));
        
        foreach ($scannedDir as $module) {
            if (is_dir($pathToModules.$module)) {
                require_once("$pathToModules/$module/mod.php");
                
                //$this->data->modules->$module = new $module;
                $this->data->modules->$module = $module;
                if (method_exists($module, "init")) {
                    $module::init();
                }
            }
        }
    }
    
    public function handleEvent($modData) {
        foreach ($this->data->modules as $module) {
            if (method_exists($module, $modData->function)) {
                $function = $modData->function;
                $module::$function($modData);
            }
        }
    }    
    
}

?>
