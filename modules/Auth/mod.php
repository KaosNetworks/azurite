<?php

class Auth extends Modules {

    public function _PRIVMSG($args) {
        $message = $args->message;
        $message = explode(" ", $message);
        $command = $message[0];
        unset($message[0]);
        
        if ($command == "login") {
            $data->user = $message[1];
            $data->pass = $message[2];
            
            $auth = Bot::kaosAPI("user", "id", $data);
            
            if ($auth->status == 1) {
                Bot::$users[$args->person->ident."@".$args->person->host] = $auth->ret;
                IRC::MSG($args->person->nick, "Login successful. Session locked to: *!{$args->person->ident}@{$args->person->host}");
            } else {
                IRC::MSG($args->person->nick, "Invalid credentials.");
            }
        } elseif ($command == "whoami") {
            foreach ($args->person as $key => $value) {
                IRC::MSG($args->person->nick, "$key = $value");
            }
        }
    }

}

?>