<?php

/**
 *  Author: Branden S. Martin (Branden.Martin@kaosnetworks.com)
 *  Creation Date: 3/21/2012
 */
class Steam extends Modules {

    public function __construct() {
        
    }

    public function _PRIVMSG($args) {
        $message = explode(" ", $args->message);
        $command = $message[0];
        unset($message[0]);
        $message = implode(" ", $message);
        $string = $message;
        
        
        if ($command == "!steam") {
            if (preg_match("/.*\+.*/", $string)) {
                $player = strstr($string, ' +', true);
            } else {
                $player = $string;
            }

            $steamID = self::findPlayer($player);

            if ($steamID == 0) {
                IRC::MSG($args->channel, "Steam account cannot be found.");
            } else {
                $Profile = self::Get_Player_Profile($steamID);
            }

            if ($Profile == 0) {
                IRC::MSG($args->channel, "Steam account cannot be found.");
            } elseif ($Profile == 1) {
                IRC::MSG($args->channel, "This person does not have a community profile.");
            } else {

                if ($Profile["Status"] == "online") {
                    $String = $Profile["Name"] . " is currently " . $Profile["Status"] . ".";
                } elseif ($Profile["Status"] == "in-game") {
                    $String = $Profile["Name"] . " is currently playing " . $Profile["Game"];
                } else {
                    $String = $Profile["Name"] . " is currently " . $Profile["Status"] . ".";
                }
                IRC::MSG($args->channel, $String);


                if (preg_match("*\+games*", $message)) {
                    $gameArray = self::Get_Player_Games($steamID);
                    $gameString = "";
                    foreach ($gameArray as $key => $gameName) {
                        $gameString .= ", $gameName";
                    }
                    $gameString = substr($gameString, 2);
                    IRC::MSG($args->channel, $gameString);
                }
            }
        } elseif ($command == "!steam->compare") {
            if ($string == "") {
                IRC::MSG($args->channel, "[Syntax] !steam->compare person1, person2 [, etc]");
            } else {
                $peoplePlus = "";
                $peopleMinus = "";
                $string = explode(", ", $string);
                $players = array();

                foreach ($string as $key => $player) {
                    $steamID = self::findPlayer($player);
                    if (strlen($steamID) != 1) {
                        $players[$key]["games"] = array();
                        $players[$key]["games"] = self::Get_Player_Games($steamID);
                        if (count($players[$key]["games"]) == 0) {
                            IRC::MSG($args->channel, "No games found for player: $player. Continuing compare for other players.");
                            unset($players[$key]);
                        } else {
                            $peoplePlus .= ", $player";
                        }
                    } else {
                        $peopleMinus .= ", $player";
                    }
                }
                                
               

                if (strlen($peopleMinus) > 1) {
                    $peopleMinus = substr($peopleMinus, 2);
                    IRC::MSG($args->channel, "Accounts NOT found: $peopleMinus.");
                }
                if (strlen($peoplePlus) > 1) {
                    $peoplePlus = substr($peoplePlus, 2);
                    IRC::MSG($args->channel, "Accounts found and will be compared: $peoplePlus.");
                }
                
                if (count($players) <= 1) {
                    IRC::MSG($args->channel, "Not enough valid accounts, need at least 2.");
                } else {
                    $gameList = $players[0]["games"];
                    unset($players[0]);
                    foreach ($players as $player) {
                        $gameList = array_intersect($gameList, $player["games"]);
                    }
                }
                
                
                $gameString = "";
                foreach ($gameList as $game) {
                    $gameString .= ", $game";
                }
                $gameString = substr($gameString, 2);
                
                if ($gameString) {
                     IRC::MSG($args->channel, $gameString);
                } else {
                     IRC::MSG($args->channel, "No games in common.");
                }
            }
        } elseif ($command == "!steam->worth") {
            if (preg_match("/.*\+.*/", $string)) {
                $player = strstr($string, ' +', true);
            } else {
                $player = $string;
            }

            $playerStats = self::Get_Account_Stats($player);

            // strip html character codes and make them IRC friendly
            $playerStats = str_replace("&#36;", chr(36), $playerStats);

            // set variables. not really needed, just makes the coding look cleaner I guess.
            $gameCount = $playerStats["GameCount"];
            $profileWorth = $playerStats["Worth"];

            // if the player exists, output information. if not, send error message.
            if ($playerStats["Status"] != 0) {
                if ($playerStats["GameCount"] > 1) {
                    IRC::MSG($args->channel, "[$player] -- Totaling $gameCount games.");
                    IRC::MSG($args->channel, "[$player] -- Profile estimated to be worth $profileWorth.");
                } elseif ($playerStats["GameCount"] == 1) {
                    IRC::MSG($args->channel, "[$player] -- Totaling only $gameCount game.");
                    IRC::MSG($args->channel, "[$player] -- Profile estimated to be worth $profileWorth.");
                } else {
                    IRC::MSG($args->channel, "[$player] -- This profile has no games at all. Cannot estimate!");
                }
            } else {
                IRC::MSG($args->channel, "Could not locate player. Are you sure you typed it correctly?");
            }
        } elseif ($command == "!steam->contrast") {
            if ($string == "") {
                IRC::MSG($args->channel, "[Syntax] !steam->contrast person1, person2, [person3, etc]");
                IRC::MSG($args->channel, "[Syntax] This will determine the games player 1 has which the rest don't. Best used for two people.");
            } else {
                $peoplePlus = "";
                $peopleMinus = "";
                $string = explode(", ", $string);
                $players = array();

                foreach ($string as $key => $player) {
                    $steamID = self::findPlayer($player);
                    if (strlen($steamID) != 1) {
                        $players[$key]["games"] = array();
                        $players[$key]["games"] = self::Get_Player_Games($steamID);
                        if (count($players[$key]["games"]) == 0) {
                            IRC::MSG($args->channel, "No games found for player: $player. Continuing contrast for other players.");
                            unset($players[$key]);
                        } else {
                            $peoplePlus .= ", $player";
                        }
                    } else {
                        $peopleMinus .= ", $player";
                    }
                }
                                
               

                if (strlen($peopleMinus) > 1) {
                    $peopleMinus = substr($peopleMinus, 2);
                    IRC::MSG($args->channel, "Accounts NOT found: $peopleMinus.");
                }
                if (strlen($peoplePlus) > 1) {
                    $peoplePlus = substr($peoplePlus, 2);
                    IRC::MSG($args->channel, "Accounts found and will be contrasted, the first player is the base of the contrast: $peoplePlus.");
                }
                
                if (count($players) <= 1) {
                    IRC::MSG($args->channel, "Not enough valid accounts, need at least 2.");
                } else {
                    $gameList = $players[0]["games"];
                    unset($players[0]);
                    foreach ($players as $player) {
                        $gameList = array_diff($gameList, $player["games"]);
                    }
                }
                
                
                $gameString = "";
                foreach ($gameList as $game) {
                    $gameString .= ", $game";
                }
                $gameString = substr($gameString, 2);
                
                if ($gameString) {
                     IRC::MSG($args->channel, $gameString);
                } else {
                     IRC::MSG($args->channel, "All players have all the same games, weird.");
                }
            }
        }
    }

    private function findPlayer($player) {

        if (preg_match("/[0-9]{17}/", $player)) {
            $return = $player;
        } else {
            $steamID = self::Get_Player_ID64($player);

            if (strlen($steamID) == 1) {
                $steamID = self::lookupBySteamDisplayName($player);
                if (strlen($steamID) == 1) {
                    $return = 0;
                } else {
                    $steamID = self::Get_Player_ID64($steamID);
                    $return = $steamID;
                }
            } else {
                $return = $steamID;
            }
        }

        return $return;
    }

    private function Get_Player_ID64($steamName) {
        $steamName = urlencode($steamName);
        $url = "http://steamcommunity.com/id/$steamName?xml=1";
        $xml = Bot::cURL($url, "xml");
        $steamID = $xml->steamID64;
        if (!$steamID) {
            $steamID = 0;
        }
        return $steamID;
    }

    private function Get_Player_Profile($steamID) {
        $url = "http://steamcommunity.com/profiles/$steamID?xml=1";
        $xml = Bot::cURL($url, "xml");

        $profile = array();

        if ($xml->steamID64) {
            $profile["Name"] = $xml->steamID;
            $profile["SteamID64"] = $xml->steamID64;
            $profile["Status"] = $xml->onlineState;
            $profile["StatusMessage"] = $xml->stateMessage;
            $profile["MemberSince"] = $xml->memberSince;
            $profile["Rating"] = $xml->steamRating;
            $profile["Hours"] = $xml->hoursPlayed2Wk;

            $profile["Game"] = $xml->inGameInfo->gameName;
            $profile["ServerIP"] = $xml->inGameServerIP;
        } else {
            $profile = 0;
        }

        if (preg_match("/This user has not yet set up their Steam Community profile/", $xml->privacyMessage)) {
            $profile = 1;
        }

        return $profile;
    }

    function lookupBySteamDisplayName($name) {
        $name = urlencode($name);
        $url = "http://steamcommunity.com/actions/Search?K=\"$name\"";

        $string = Bot::cURL($url, "string");

        if (preg_match("/\<a class\=\"linkTitle\" href\=\"http\:\/\/steamcommunity\.com\/id\/(.*)\"\>(.*)\<\/a\>/", $string, $matches)) {
            $steamID = $matches[1];
        } else {
            $steamID = 0;
        }


        return $steamID;
    }

    function Get_Player_Games($Steam_ID) {
        $url = "http://steamcommunity.com/profiles/$Steam_ID/games?xml=1&tab=all";
        $xml = Bot::cURL($url, "xml");

        $Games = array();

        foreach ($xml->games->game as $key => $game) {
            $games[] = "$game->name";
        }

        return $games;
    }

    private function Get_Player_Groups($Steam_ID) {
        $url = "";

        $xml = Bot::cURL($url, "xml");
    }

    private function Get_Player_Stats($Steam_ID) {
        $url = "";

        $xml = Bot::cURL($url, "xml");
    }

    private function Get_Group_List() {
        $url = "";

        $xml = Bot::cURL($url, "xml");
    }

    private function Get_Account_Stats($Steam_ID) {
        $html = file_get_contents("http://www.steamcalculator.com/id/$Steam_ID");
        if (preg_match_all('/\<div id\=\"rightdetail\"\>Found (.*?) Games with a value of \<h1\>(.*?) USD\<\/h1\>\<\/div\>/s', $html, $stats, PREG_SET_ORDER)) {

            $profileStats = array();

            foreach ($stats as $stat) {
                $profileStats["GameCount"] = $stat[1];
                $profileStats["Worth"] = $stat[2];
            }
            $profileStats["Status"] = 1;
        } else {
            $profileStats["Status"] = 0;
        }
        return $profileStats;
    }

}

?>