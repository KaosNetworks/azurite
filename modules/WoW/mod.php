<?php

class WoW extends Modules {

    const _apiURL = "http://us.battle.net/api/wow";

    public function init() {
        
    }

    public function _PRIVMSG($args) {
        $message = $args->message;
        $message = explode(" ", $message);
        $command = $message[0];
        unset($message[0]);
        $message = implode(" ", $message);

        if ($command == "!wow->char") {
            $message = explode(",", $message);
            $realm = $message[0];
            $name = $message[1];
            $chara = self::_getCharacterInfo($realm, $name);
            
            if ($chara->status == 1) {
                $chara = $chara->char;
                $string = "{$chara->name} is a level {$chara->level} {$chara->race->name} ({$chara->race->side}) {$chara->class->name}.";
                if ($chara->guild->name) {
                    $string .= " {$chara->name} is in the {$chara->guild->name} guild, which has {$chara->guild->members} members.";
                }
            } else {
                $string = $chara->msg;
            }
            
            IRC::MSG($args->channel, $string);
        }
    }

    private function _getCharacterInfo($realm, $name) {
        $data = new stdClass();
        $data->realm = trim($realm);
        $data->name = trim($name);
        $json = Bot::kaosAPI("wow", "getCharacter", $data);
        print_r($json);
        return $json;
    }

}

?>