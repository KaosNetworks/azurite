<?php

class Cache extends Modules {

    public function init() {
        BOT::$cache->channels = array();
    }

    function IRC_EVENT_352($args) {
        if (!isset(BOT::$cache->channels[$args->channel]->users)) {
            BOT::$cache->channels[$args->channel]->users = array();
        }

        $modes = array();
        if (preg_match("/([~&@%])/", $args->mode)) {
            $modes["op"] = 1;
        } else {
            $modes["op"] = 0;
        }

        if (preg_match("/([+])/", $args->mode)) {
            $modes["voice"] = 1;
        } else {
            $modes["voice"] = 0;
        }

        BOT::$cache->channels[$args->channel]->users[$args->nick] = new stdClass();
        BOT::$cache->channels[$args->channel]->users[$args->nick]->lastTalk = 0;
        BOT::$cache->channels[$args->channel]->users[$args->nick]->modes = $modes;
    }

    function _PRIVMSG($args) {
        BOT::$cache->channels[$args->channel]->users[$args->person->nick]->lastTalk = time();
    }

    function _PART($args) {
        unset(BOT::$cache->channels[$args->channel]->users[$args->person->nick]);
    }

    function _JOIN($args) {
        if ($args->person->nick == $args->config->bot->nick) {
            IRC::WHO($args->channel);
        }

        BOT::$cache->channels[$args->channel]->users[$args->person->nick] = new stdClass();
        BOT::$cache->channels[$args->channel]->users[$args->person->nick]->lastTalk = 0;
        BOT::$cache->channels[$args->channel]->users[$args->person->nick]->modes["voice"] = 0;
        BOT::$cache->channels[$args->channel]->users[$args->person->nick]->modes["op"] = 0;
    }

}

?>