<?php

class Weather extends Modules {

    public function init() {
        
    }

    public function _PRIVMSG($args) {
        $Param = explode(" ", $args->message);
        $Command = $Param[0];
        unset($Param[0]);

        if ($Command == "!weather") {
            $Search = implode(" ", $Param);
            $Zip = rawurlencode($Search);

            // Grab the forecast data for said location
            $url = "http://api.kaosnetworks.com/weather/?type=json&city=$Zip";
            $json = Bot::cURL($url, "json");
            $json = $json['weather'];

            // Setting variables for the grabbed forecast data
            $Location = $json[0]["current"]["location"];
            $Condition = $json[0]["current"]["condition"];
            $TempF = $json[0]["current"]["temp"];
            $TempC = $json[0]["current"]["temp_c"];
            $Humidity = $json[0]["current"]["humidity"];
            $Wind = $json[0]["current"]["wind_condition"];

            // Check to see if location exists
            if ($Location == "") {
                Core::MSG($Where = $Dest, $What = "Error trying to locate said place. Are you sure it exists?");
            } else {
                IRC::MSG($args->channel, "Current forecast for $Location");
                IRC::MSG($args->channel, "Condition: $Condition -- Temp: $TempF" . chr(176) . "F ($TempC" . chr(176) . "C) -- Humidity: $Humidity");
                IRC::MSG($args->channel, "Wind: $Wind");
            }
        } elseif ($Command == "!weather->tomorrow") {
            $Search = implode(" ", $Param);
            $Zip = rawurlencode($Search);

            $tomorrowshort = date('l', time() + 86400);
            $tomorrowfull = date('l', time() + 86400);

            // Grab the forecast data for said location
            $url = "http://api.kaosnetworks.com/weather/?type=json&city=$Zip";
            $json = Bot::cURL($url, "json");
            $json = $json['weather'];

            // Setting variables for the grabbed forecast data
            $Location = $json[0]["current"]["location"];
            $Condition = $json[0]["1"]["condition"];
            $HighTemp = $json[0]["1"]["high"];
            $LowTemp = $json[0]["1"]["low"];

            // Check to see if location exists
            if ($Location == "") {
                IRC::MSG($args->channel, "Error trying to locate said place. Are you sure it exists?");
            } else {
                IRC::MSG($args->channel, "$tomorrowfull's forecast for $Location");
                IRC::MSG($args->channel, "Condition: $Condition");
                IRC::MSG($args->channel, "High Temp: $HighTemp" . chr(176) . "F -- Low Temp: $LowTemp" . chr(176) . "F");
            }
        }
    }

}

?>