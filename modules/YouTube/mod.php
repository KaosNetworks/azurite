<?php

class YouTube extends Modules {

    public function init() {
        
    }

    public function _PRIVMSG($args) {

        if (preg_match('/youtube\.com\/watch\?v\=([A-Za-z0-9\-_]+)&?.*/', $args->message, $matches)) {
            $URL = "http://gdata.youtube.com/feeds/api/videos/" . $matches[1] . "?v=2&alt=jsonc";
            $ch = curl_init($URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
            $data = curl_exec($ch);
            curl_close($ch);
            $xml = json_decode($data);

            $title = $xml->data->title;
            $likes = $xml->data->likeCount;
            $dislikes = $xml->data->ratingCount - $xml->data->likeCount;
            $views = $xml->data->viewCount;
            $category = $xml->data->category;
            $author = $xml->data->uploader;
            $uploaded = $xml->data->recorded;


            IRC::MSG($args->channel, "[1,0You0,4Tube] $title [+$likes] [-$dislikes] [$views\\views] [$category] [$author - $uploaded]");
        }
    }

}

?>