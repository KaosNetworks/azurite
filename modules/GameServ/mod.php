<?php

class GameServ extends Modules {

    private $gameServers;

    public function init() {
        require_once 'rcon.class.php';
        $this->gameServers = new stdClass();
        //$this->gameServers->garrysMod = new rcon("gmod.kaosrealm.com", 27015, "K@0s4Dm1n1stRAT1on");
        //$this->gameServers->garrysMod = new rcon("zeus.kaosnetworks.com", 27015, "K@0s4Dm1n1stRAT1on");
        //$this->gameServers->garrysMod->Auth();
    }

    public function a_PRIVMSG($args) {
        if ($args->channel == "#kaosbot") {
            $this->gameServers->garrysMod->rconCommand("say [{$args->person->nick}] {$args->message}");
        }
        if (preg_match("/!status/", $args->message)) {
            $status = $this->gameServers->garrysMod->rconCommand("status");
            IRC::MSG("#kaosbot", "$status");
            var_dump ($status);
        }
    }

    public function a_SAY($args) {
        if ($args->person["name"] != "Console") {
            IRC::MSG("#kaosbot", "<{$args->person["name"]}> {$args->message}");
        }
    }

    public function a_DISCONNECTED($args) {
        IRC::MSG("#kaosbot", "{$args->person["name"]} ragequit the game.");
    }

    public function a_CONNECTED($args) {
        IRC::MSG("#kaosbot", "{$args->person["name"]} connected to the game server.");
    }

}

?>