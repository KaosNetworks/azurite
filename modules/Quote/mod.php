<?php

class Quote extends Modules {

    public function _PRIVMSG($args) {
        $message = $args->message;
        $message = explode(" ", $message);
        $command = $message[0];
        unset($message[0]);

        if ($command == "!quote->rand") {
            $data = new stdClass();
            $data->type = "rand";

            $getQuote = Bot::kaosAPI("quotes", "get", $data);

            if ($getQuote->status == 1) {
                $getQuote = $getQuote->quote;
                $quote = $getQuote->quote;
                $quote = explode("\n", $quote);

                IRC::MSG($args->channel, "#{$getQuote->id} posted by {$getQuote->username} on " . date('m/d/Y @ h:m', strtotime($getQuote->postedOn)) . " in {$getQuote->channel}");
                foreach ($quote as $line) {
                    IRC::MSG($args->channel, str_replace('<br />', "", html_entity_decode($line)));
                }
            } else {
                IRC::MSG($args->person->nick, "Something broke. :[");
            }
        } elseif ($command == "!quote->id") {
            $data = new stdClass();
            $data->type = "id";
            $data->id = $message[1];

            $getQuote = Bot::kaosAPI("quotes", "get", $data);

            if ($getQuote->status == 1) {
                $getQuote = $getQuote->quote;
                $quote = $getQuote->quote;
                $quote = explode("\n", $quote);

                IRC::MSG($args->channel, "#{$getQuote->id} posted by {$getQuote->firstName} {$getQuote->lastName} on " . date('m/d/Y @ h:m', strtotime($getQuote->postedOn)) . " in {$getQuote->channel}");
                foreach ($quote as $line) {
                    IRC::MSG($args->channel, str_replace('<br />', "", html_entity_decode($line)));
                }
            } else {
                IRC::MSG($args->person->nick, "Something broke. :[");
            }
        } elseif ($command == "!quote") {
            if ($args->person->isLoggedIn) {
                $data = new stdClass();
                $data->quote = str_replace('||', "<br \>", implode(" ", $message));
                $data->userID = $args->person->id;
                $data->channel = $args->channel;

                $addQuote = Bot::kaosAPI("quotes", "add", $data);

                if ($addQuote->status == 1) {
                    IRC::MSG($args->channel, "Quote added, please see latest quotes @ http://kaosnetworks.com/quotes");
                } else {
                    IRC::MSG($args->channel, "Something broke. :[");
                }
            } else {
                IRC::MSG($args->channel, "You must login to do that. /msg {$args->config->bot->nick} login <username> <password>");
            }
        }
    }

}

?>