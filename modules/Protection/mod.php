<?php

class Protection extends Modules {

    const _reportChannel = "##KaosBot##";

    public function init() {
        
    }

    public function _KICK($args) {
        if ($args->kickee == $args->config->bot->nick) {
            IRC::MSG($args->person->nick, "I am a network official bot, please do not kick me from any channel on this network. This action has been logged and reported.");
            IRC::MSG(self::_reportChannel, $args->person->nick . " tried to kick me from: " . $args->channel . ".");
            IRC::JOIN($args->channel);
        }
    }

}

?>