<?php

class TV extends Modules {

    public function _PRIVMSG($args) {
        $message = $args->message;
        $message = explode(" ", $message);
        $command = $message[0];
        unset($message[0]);
        $showName = implode(" ", $message);
        $show = urlencode($showName);

        if ((($command == "!ep") || ($command == "!show")) && ($showName == "")) {
            IRC::MSG($args->channel, "You did not specify a show.");
        } else {

            if ($command == "!show") {
                $showID = self::Get_Show_ID($show);
                $showInfo = self::Get_Show_Info($showID);

                if ($showInfo["Status"] != "Canceled/Ended") {
                    IRC::MSG($args->channel, "{$showInfo["Name"]} ({$showInfo["Year"]}) is a {$showInfo["Country"]} {$showInfo["Status"]} on {$showInfo["Station"]}. It is currently airing on {$showInfo["AirDay"]} at {$showInfo["AirTime"]}. {$showInfo["Name"]} is currently on season {$showInfo["Seasons"]}.");
                } else {
                    IRC::MSG($args->channel, "{$showInfo["Name"]} was a show on {$showInfo["Station"]} that started {$showInfo["StartDate"]} and ended on {$showInfo["EndDate"]}. {$showInfo["Name"]} lasted {$showInfo["Seasons"]} seasons.");
                }
            } elseif ($command == "!ep") {

                $Episode_Info = self::Get_Episode_List($show);

                IRC::MSG($args->channel, "Last Episode: ({$Episode_Info["Last"]["1"]}) \"{$Episode_Info["Last"][2]}\" aired: {$Episode_Info["Last"][3]}");
                if ($Episode_Info["Next"][3]) {
                    IRC::MSG($args->channel, "Next Episode: ({$Episode_Info["Next"]["1"]}) \"{$Episode_Info["Next"][2]}\" airing: {$Episode_Info["Next"][3]}");
                } else {
                    IRC::MSG($args->channel, "Show is either canceled or next episode not available.");
                }
            }
        }
    }

    private function Get_Show_ID($Show) {
        $url = "http://services.tvrage.com/feeds/search.php?show=$Show&results=1";

        $xml = self::cURL($url);
        $Show_ID = $xml->show->showid;

        return $Show_ID;
    }

    private function Get_Show_Info($Show_ID) {
        $url = "http://services.tvrage.com/feeds/showinfo.php?sid=$Show_ID&results=1";

        $xml = self::cURL($url);

        $Show_Info = array();
        $Show_Info["Name"] = $xml->showname;
        $Show_Info["Seasons"] = $xml->seasons;
        $Show_Info["Year"] = $xml->started;
        $Show_Info["StartDate"] = $xml->startdate;
        $Show_Info["EndDate"] = $xml->ended;
        $Show_Info["Status"] = $xml->status;
        $Show_Info["AirTime"] = $xml->airtime;
        $Show_Info["AirDay"] = $xml->airday;
        $Show_Info["Station"] = $xml->network;
        $Show_Info["Country"] = $xml->origin_country;

        return $Show_Info;
    }

    private function Get_Episode_List($Show) {

        $url = "http://services.tvrage.com/tools/quickinfo.php?show=$Show";

        $xml = self::cURL($url, "string");

        $Episode_Info = array();

        $Get_Episode_List = split("\n", $xml);

        $LastEpisode = $Get_Episode_List[6];
        $LastEpisode = preg_split("/[@^]/", $LastEpisode);
        $NextEpisode = $Get_Episode_List[7];
        $NextEpisode = preg_split("/[@^]/", $NextEpisode);

        $Episode_Info["Last"] = $LastEpisode;
        $Episode_Info["Next"] = $NextEpisode;

        return $Episode_Info;
    }
    



}

?>