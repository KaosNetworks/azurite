<?php

class AntiIdle extends Modules {

    public function init() {
        
    }

    public function IRC_PING($args) {
        foreach (BOT::$cache->channels["#kaos"]->users as $nick => $user) {
            if ($nick != "GLaDOS") {
                if ((time()-$user->lastTalk > $args->config->antiIdle->deVoice || $user->lastTalk == 0) && @$user->modes["voice"] == 1) {
                    IRC::SAMODE("#Kaos", "-v", $nick);
                    BOT::$cache->channels["#kaos"]->users[$nick]->modes["voice"] = 0;
                }
                if ((time()-$user->lastTalk > $args->config->antiIdle->deOp || $user->lastTalk == 0) && @$user->modes["op"] == 1) {
                    IRC::SAMODE("#Kaos", "-qaoh", "$nick $nick $nick $nick");
                    BOT::$cache->channels["#kaos"]->users[$nick]->modes["op"] = 0;
                }
            }
        }
    }
    
    public function _PRIVMSG($args) {
        if ($args->message == "!idlecheck") {
            self::IRC_PING($args);
        }
        
        if ($args->channel == "#kaos" && BOT::$cache->channels["#kaos"]->users[$args->person->nick]->modes["voice"] == 0) {
            BOT::$cache->channels["#kaos"]->users[$args->person->nick]->modes["voice"] = 1;
            IRC::MODE("#Kaos", "+v", $args->person->nick);
        }
    }

}

?>